from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Predictions of wether the client will initiate an auto insurance claim.',
    author='Jakob',
    license='',
)
